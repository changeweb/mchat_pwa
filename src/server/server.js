const express = require('express')
// var bodyParser = require('body-parser');
const app = express()
const compression = require('compression');
const port = 3000
var httpRaw = require('http')
var http = httpRaw.Server(app)
var io = require('socket.io')(http, {
    pingTimeout: 60000,
});

var LinkPreview = require('link-preview-js');

app.use(compression());
app.use(express.static('public'))
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

var serverRoutes = require('./routes/servers')
app.use('/api/servers/', serverRoutes)

app.get('/getIceServers', (req, res) => {
    let config = {
        urls: ["stun:ss-turn2.xirsys.com"],
        username: "SV1SkYgkFl2Ugd8QcqySZB2GvDRu-eSq66X0mvcLtKOu73HyIaRidVBQg6Ga28iWAAAAAF1ho-BoYXNpYjAwOA==",
        credential: "47ea8378-c6b1-11e9-be73-322c48b34491",
        turn_urls: [
            "turn:ss-turn2.xirsys.com:80?transport=udp",
            //"turn:ss-turn2.xirsys.com:3478?transport=udp",
            "turn:ss-turn2.xirsys.com:80?transport=tcp",
            //"turn:ss-turn2.xirsys.com:3478?transport=tcp",
            // "turns:ss-turn2.xirsys.com:443?transport=tcp",
            // "turns:ss-turn2.xirsys.com:5349?transport=tcp"
        ]
    }
    res.send(config)
})

var mySocketRoom = ''
var clients = 0
var callAccepted = false

io.on('connection', function (socket) {
    console.log("TCL: socket connected!")

    // Join all subscribed rooms
    socket.join(['room1/1','room2/1'])

    var currentRoom, id;
    var currentVideoCallingRoom;
    // initially join my room
    if (mySocketRoom == '') {
        mySocketRoom = 'saro'
    } else if (mySocketRoom == 'saro') {
        mySocketRoom = 'mim'
    } else {
        console.log('2 already joined')
        return;
    } // get from db

    socket.on('join_my_room', (data, fn) => {
        console.log('mySocketRoom', mySocketRoom)
        socket.join(mySocketRoom)
        currentRoom = mySocketRoom
        fn(currentRoom)
    })

    // -- Text messaging starts
    socket.on('message',(data) => {
        console.log('message',data)
        io.to(data.room).emit('newMessage', data)
    })
    // -- Text messaging ends
    // -- Gif starts
    socket.on('searchGif', data => {
        console.log('gif',data.gifInput)
        let term = encodeURIComponent(data.gifInput)
        let url = 'http://api.giphy.com/v1/gifs/search?q=' + term + '&api_key=hYHSO8RYFASEyIsPVKQnG2FU74Zmlajj&limit=10';
        httpRaw.get(url, (response) => {
                // SET ENCODING OF RESPONSE TO UTF8
                response.setEncoding('utf8');
                let body = '';
                // listens for the event of the data buffer and stream
                response.on('data', (d) => {
                    // CONTINUOUSLY UPDATE STREAM WITH DATA FROM GIPHY
                    body += d;
                });
                // once it gets data it parses it into json 
                response.on('end', () => {
                    // WHEN DATA IS FULLY RECEIVED PARSE INTO JSON
                    let parsed = JSON.parse(body);
                    // RENDER THE HOME TEMPLATE AND PASS THE GIF DATA IN TO THE TEMPLATE
                    socket.emit('search-giphy', { gifs: parsed.data })
                });
            });
    })
    // -- Gif ends
    // -- Sticker starts
    socket.on('searchSticker', data => {
        console.log('sticker',data.stickerInput)
        let term = encodeURIComponent(data.stickerInput)
        let url = 'http://api.giphy.com/v1/stickers/search?q=' + term + '&api_key=hYHSO8RYFASEyIsPVKQnG2FU74Zmlajj&limit=10';
        httpRaw.get(url, (response) => {
                // SET ENCODING OF RESPONSE TO UTF8
                response.setEncoding('utf8');
                let body = '';
                // listens for the event of the data buffer and stream
                response.on('data', (d) => {
                    // CONTINUOUSLY UPDATE STREAM WITH DATA FROM GIPHY
                    body += d;
                });
                // once it gets data it parses it into json 
                response.on('end', () => {
                    // WHEN DATA IS FULLY RECEIVED PARSE INTO JSON
                    let parsed = JSON.parse(body);
                    // RENDER THE HOME TEMPLATE AND PASS THE GIF DATA IN TO THE TEMPLATE
                    socket.emit('search-giphy-sticker', { stickers: parsed.data })
                });
            });
    })
    // -- Sticker ends

    // -- Link preview starts
    socket.on('link-preview', async data => {
        console.log(data)
        await LinkPreview.getPreview(data)
        .then(res => {
            console.log('c')
            socket.emit(`getLinkPreview`, res)
        }).catch(e => {
            console.log(e)
            socket.emit(`getLinkPreview`, '')
        });
    })
    // -- Link preview ends

    // -- Video calling starts
    socket.on('start_video_call', (data) => {
        // clients++
        // currentRoom = data.room
        // if (clients == 1) {
        //     //io.to('saro').emit('CreatePeer')
        //     socket.emit('CreatePeer')
        //     console.log('CreatePeer')
        // }
        // // io.to(data.room).emit('friend_video_calling', data.sender)
        if (clients < 2) {
            if (clients == 1) {
                socket.emit('CreatePeer')
            }
            if(!callAccepted){
                // needs from database
                currentVideoCallingRoom = 'saro'
                io.to(data.room).emit('friend_video_calling', data.sender)
            }
        } else {
            socket.emit('SessionActive')
        }
        ++clients
    })

    socket.on('Offer', SendOffer)
    socket.on('Answer', SendAnswer)

    function SendOffer(offer) {
        console.log('BackOffer')
        this.broadcast.emit('BackOffer', offer)
    }

    function SendAnswer(data) {
        console.log('BackAnswer')
        this.broadcast.emit('BackAnswer', data)
    }

    socket.on('video_call_accepted', (data) => {
        // join friend's room
        // auth as friend required
        // check if already 2 joined. if true: occupied, call later.
        //socket.join(data.room)
        callAccepted = true
        io.to(data.room).emit('callee_accepted_video_call')
    })

    socket.on('end_video_calling', (data) => {
        clients = 0
        io.to(currentRoom).emit('video_call_end', 'Call ends')
        currentRoom = mySocketRoom
    })

    // socket.on('end_active_video_call', (data) => {
    //     // if(data.room !== mySocketRoom){
    //     //     socket.leave(data.room)
    //     // }
    //     io.to(currentRoom).emit('video_call_end', 'Call ends')
    //     currentRoom = mySocketRoom
    // })

    // -- Video calling ends

    socket.on('disconnect', () => {
        io.to(currentRoom).emit('friend_disconnected', {
            friend: ''
        })
    })
    // socket.on('my other event', function (msg) {
    //     console.log('data', msg)
    //     io.emit('msg', msg)
    // });
    // socket.on('bobmsg', function (msg) {
    //     console.log('data', msg)
    //     io.emit('msg2', msg)
    // });
});

// app.get('/', (req, res) => res.send('Hello World!'))
var server = http.listen(port, () => {
    console.log('server is running on port', server.address().port)
})
// const puppeteer = require('puppeteer');
// const fetch = require('node-fetch');
// const dropcss = require('dropcss');
// const fs = require('fs');
// (async () => {
//     const browser = await puppeteer.launch();
//     const page = await browser.newPage();
//     await page.goto('http://localhost:3000/index.html');
//     const html = await page.content();
//     const styleHrefs = await page.$$eval('link[rel=stylesheet]', els => Array.from(els).map(s => s.href));
//     await browser.close();

//     await Promise.all(styleHrefs.map(href =>
//         fetch(href).then(r => r.text()).then(css => {
//             let start = +new Date();

//             let clean = dropcss({
//                 css,
//                 html,
//             });

//             fs.writeFile(__dirname+'/../../public/test'+start+'.css', clean.css, function(err) {
//                 if(err) {
//                     return console.log(err);
//                 }
//                 console.log("The test"+start+" file was saved!");
//             });

//             console.log({
//                 stylesheet: href,
//                 cleanCss: clean.css,
//                 elapsed: +new Date() - start,
//             });
//         })
//     ));

//     server.close();
// })();