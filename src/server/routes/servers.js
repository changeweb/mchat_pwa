var express = require('express');
var router = express.Router();
var servers = require('../controllers/servers')

router.get('/public/:serverId', servers.pub);

// router.post('/community', express.json({type: 'application/json'}), communities.createPub);

module.exports = router;