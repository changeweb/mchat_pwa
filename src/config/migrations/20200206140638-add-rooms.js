'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('rooms', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
        length: 10,
        notNull: true,
      },
      name: 'string',
      summary: 'string',
      socket_room: 'string',
      type: 'string', // privacy
      status: 'string',
      tag: 'int',
      category_id: {
        foreignKey: {
          name: 'category_id_fk',
          table: 'categories',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      created_at: 'datetime'
    },
    ifNotExists: true
  });
};

exports.down = function(db) {
  return db.dropTable('rooms');
};

exports._meta = {
  "version": 1
};
