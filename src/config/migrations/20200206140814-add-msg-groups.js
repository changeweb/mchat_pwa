'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('msg_groups', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
        length: 10,
        notNull: true,
      },
      creator_id: {
        foreignKey: {
          name: 'creator_id_fk',
          table: 'users',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      socket_room: 'string',
      status: 'string',
      tag: 'int',
      created_at: 'datetime'
    },
    ifNotExists: true
  });
};

exports.down = function(db) {
  return db.dropTable('msg_groups');
};

exports._meta = {
  "version": 1
};
