'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('servers', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
        length: 10,
        notNull: true,
      },
      name: 'string',
      summary: 'string',
      type: 'string',
      server_img_src: 'string',
      status: 'string',
      invite_code: 'string',
      verified: 'boolean',
      owner_id: {
        foreignKey: {
          name: 'user_id_fk',
          table: 'users',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      created_at: 'datetime'
    },
    ifNotExists: true
  });
};

exports.down = function(db) {
  return db.dropTable('servers');
};

exports._meta = {
  "version": 1
};
