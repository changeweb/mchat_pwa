'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('friends', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true,
        unsigned: true,
        length: 10,
        notNull: true,
      },
      my_id: {
        foreignKey: {
          name: 'my_id_fk',
          table: 'users',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      friend_id: {
        foreignKey: {
          name: 'friend_id_fk',
          table: 'users',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      status: 'string', // friend, pending, blocked
      created_at: 'datetime'
    },
    ifNotExists: true
  });
};

exports.down = function(db) {
  return db.dropTable('friends');
};

exports._meta = {
  "version": 1
};
