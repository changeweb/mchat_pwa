import ChatIndex from './pages/chat/index.svelte'
import FriendVideoPage from './pages/chat/friendVideo.svelte'
import VideoCallPage from './pages/chat/videoCall.svelte'
import MePage from './pages/me/index.svelte'
import ServersPage from './pages/servers/index.svelte'
import NotFound from './pages/error/notFound.svelte'

export default {
    '/': MePage,
    '/servers': ServersPage,
    '/chat/:serverId/:roomId': ChatIndex,
    '/friend/:friendId': FriendVideoPage,
    '/videoCall/1': VideoCallPage,
    '*': NotFound
}