import { writable, readable } from 'svelte/store';
import io from 'socket.io-client'

export const socket = io()
export const currentVideoCallingRoom = writable('')
export const mySocketRoom = writable('')
export const videoStream = writable(0)
// export const videoPageVisited = writable(false)
export const sharingScreen = writable(false)
export const draggableVideoVisible = writable(false)
export const callAccepted = writable(false);
export const callActive = writable(false);
export const activeServerId = writable(0);
export const currentRouteParams = writable(null)

export const isMobile = readable(window.matchMedia("only screen and (max-width: 760px)").matches);