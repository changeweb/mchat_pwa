export let invokeGetDisplayMedia = (success,error) => {
    let displayMediaStreamConstraints = {
        video: true, // or pass HINTS
        audio: true
    };
    if (navigator.mediaDevices.getDisplayMedia) {
        navigator.mediaDevices.getDisplayMedia(displayMediaStreamConstraints).then(success).catch(error);
    } else {
        navigator.getDisplayMedia(displayMediaStreamConstraints).then(success).catch(error);
    }
}

export let addStreamStopListener = (stream, callback) => {
    stream.addEventListener('ended', function() {
        callback();
        callback = function() {};
    }, false);
    stream.addEventListener('inactive', function() {
        callback();
        callback = function() {};
    }, false);
    stream.getTracks().forEach(function(track) {
        track.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);
        track.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);
    });
}