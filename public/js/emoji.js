//import EmojiButton from '@joeattardi/emoji-button';
let loadEmoji = new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.async = true
    script.src = `/js/emoji-button.min.js`
    script.onload = resolve
    script.onerror = reject
    if (document.head.lastChild.src !== script.src) {
        document.head.appendChild(script)
    } else {
        resolve()
    }
})
loadEmoji.then(() => {
    const button = document.getElementById('emoji-button');
    const picker = new EmojiButton({
        position: 'top-end'
    });

    picker.on('emoji', emoji => {
        document.getElementById('chatTextarea').value += emoji;
    });

    button.addEventListener('click', () => {
        picker.pickerVisible ? picker.hidePicker() : picker.showPicker(button);
    });

    window.emojiScriptLoaded = true

    button.click()
})