try {
    // Try something wrong here
    importScripts('/third_party/workbox/workbox-v4.3.1/workbox-sw.js');
    workbox.setConfig({
        modulePathPrefix: '/third_party/workbox/workbox-v4.3.1/'
      });
    if (workbox) {
        console.log(`Yay! Workbox is loaded 🎉`);
        // Force development builds
        workbox.setConfig({
            debug: true
        });
        workbox.routing.registerRoute(
            new RegExp('\\.js$'),
            new workbox.strategies.NetworkFirst({
                cacheName: 'js-cache',
                plugins: [
                    new workbox.expiration.Plugin({
                        maxEntries: 10,
                        maxAgeSeconds: 60 * 60 * 24 * 365, //5 * 60, // 5 minutes
                    }),
                ],
            })
        );
        workbox.routing.registerRoute(
            // Cache CSS files.
            new RegExp('\\.css$'),
            // Use cache but update in the background.
            new workbox.strategies.StaleWhileRevalidate({
                // Use a custom cache name.
                cacheName: 'css-cache',
                plugins: [
                    new workbox.expiration.Plugin({
                        maxEntries: 10,
                        maxAgeSeconds: 60 * 60 * 24 * 365, //5 * 60, // 5 minutes
                    }),
                ],
            })
        );

        // var networkFirst = new workbox.strategies.NetworkFirst({
        //     cacheName: 'outside-pages',
        //     plugins: [
        //         new workbox.expiration.Plugin({
        //             maxEntries: 20,
        //             maxAgeSeconds: 60 * 60 * 24 * 365, //5 * 60, // 5 minutes
        //         }),
        //     ],
        //   });
          
        //   const customHandler = async (args) => {
        //     try {
        //         const response = await networkFirst.handle(args);
        //             return response || await caches.match('./offline.html').then(function (response) {
        //                 return response || fetch(event.request);
        //         });
        //     } catch (error) {
        //         return await caches.match('./offline.html').then(function (response) {
        //             return response || fetch(event.request);
        //         });
        //     }
        //   };
          
        //   workbox.routing.registerRoute(
        //     /\/|index.html|offline.html/, 
        //     customHandler
        //   );

        // workbox.routing.registerRoute(
        //     new RegExp('/.|index.html|offline.html/'),
        //     async ({
        //         event
        //     }) => {
        //         try {
        //             return await workbox.strategies.networkFirst({
        //                 cacheName: 'html-pages',
        //                 plugins: [
        //                     new workbox.expiration.Plugin({
        //                         maxEntries: 10,
        //                         maxAgeSeconds: 60 * 60 * 24 * 365, //5 * 60, // 5 minutes
        //                     }),
        //                 ],
        //             }).handle({
        //                 event
        //             }) || await caches.match('/offline.html').;
        //         } catch (error) {
        //             return caches.match('/offline.html');
        //         }
        //     }
        // );
    } else {
        console.log(`Boo! Workbox didn't load 😬`);
    }
} catch (e) {
    console.error(e.message);
}
importScripts('/third_party/cache-polyfill.js');


// self.addEventListener('install', function (e) {
//     e.waitUntil(
//         caches.open('4sparrow').then(function (cache) {
//             return cache.addAll([
//                 '/',
//                 '/index.html',
//                 '/favicon.png',
//                 '/apple-touch-icon.png',
//                 '/manifest.json',
//                 '/uikit-3.2.6/css/uikit.min.css',
//                 '/global.css',
//                 '/bundle.css',
//                 '/uikit-3.2.6/js/uikit.min.js',
//                 '/uikit-3.2.6/js/uikit-icons.min.js',
//                 '/js/simplepeer.min.js',
//                 '/bundle.js'
//             ]);
//         })
//     );
// });
self.addEventListener('fetch', function (event) {
    console.log(event.request.url);
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});